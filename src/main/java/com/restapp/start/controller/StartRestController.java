package com.restapp.start.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/start")
public class StartRestController {

    //add code test
    @GetMapping("/home")
    public String homePage (){
        return "Hello home page";
    }

}
