package com.restapp.start.controller;

import com.restapp.start.exception.StudentErrorMessage;
import com.restapp.start.exception.StudentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class StudentRestExceptionHandler {

    //add exception method

    @ExceptionHandler
    public ResponseEntity<StudentErrorMessage> handleException(StudentNotFoundException error){

        //create student error response
        StudentErrorMessage message = new StudentErrorMessage();
        message.setStatus(HttpStatus.NOT_FOUND.value());
        message.setMessage("Student not found !");
        message.setTimeStamp(System.currentTimeMillis());
        //return response entity
        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<StudentErrorMessage> handleException(Exception error){

        //create student error response
        StudentErrorMessage message = new StudentErrorMessage();
        message.setStatus(HttpStatus.BAD_REQUEST.value());
        message.setMessage(error.getMessage());
        message.setTimeStamp(System.currentTimeMillis());
        //return response entity
        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }
}
