package com.restapp.start.controller;

import com.restapp.start.entity.Student;
import com.restapp.start.exception.StudentErrorMessage;
import com.restapp.start.exception.StudentNotFoundException;
import jakarta.annotation.PostConstruct;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentRestController {

    private List<Student> theStudents;

    @PostConstruct
    public void loadData(){
        theStudents = new ArrayList<>();
        //add
        theStudents.add(new Student("James", "Patel"));
        theStudents.add(new Student("Marcus", "Delgado"));
        theStudents.add(new Student("Mary", "Gregoire"));
    }

    @GetMapping("/students")
    public List<Student> getStudents(){
        return theStudents;
    }

    @GetMapping("/students/{studentId}")
    public Student getStudents(@PathVariable int studentId){
        //just try to retrieve the data
        if((studentId >= theStudents.size()) || studentId < 0){
            throw new StudentNotFoundException("Student not found " + studentId);
        }
        return theStudents.get(studentId);
    }

//    @ExceptionHandler
//    public ResponseEntity<StudentErrorMessage> handleException(StudentNotFoundException error){
//
//        //create student error response
//        StudentErrorMessage message = new StudentErrorMessage();
//        message.setStatus(HttpStatus.NOT_FOUND.value());
//        message.setMessage("Student not found !");
//        message.setTimeStamp(System.currentTimeMillis());
//        //return response entity
//        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<StudentErrorMessage> handleException(Exception error){
//
//        //create student error response
//        StudentErrorMessage message = new StudentErrorMessage();
//        message.setStatus(HttpStatus.BAD_REQUEST.value());
//        message.setMessage(error.getMessage());
//        message.setTimeStamp(System.currentTimeMillis());
//        //return response entity
//        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
//    }

}
